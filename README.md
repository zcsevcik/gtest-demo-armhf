[![pipeline status](https://gitlab.com/zcsevcik/gtest-demo-armhf/badges/master/pipeline.svg)](https://gitlab.com/zcsevcik/gtest-demo-armhf/commits/master)
[![coverage report](https://gitlab.com/zcsevcik/gtest-demo-armhf/badges/master/coverage.svg)](https://gitlab.com/zcsevcik/gtest-demo-armhf/commits/master)
[![License](https://img.shields.io/badge/license-%20BSD--3-blue.svg)](./LICENSE)


# gtest-demo-armhf

C/C++ unit test demo using [Google Test](https://code.google.com/p/googletest) deployed to __GitLab CI__ with test coverage. This is a fork of the project that used Travis CI instead.

- Licensed under [BSD-3](./LICENSE)


## How to build this demo

```sh
git clone --recursive https://gitlab.com/zcsevcik/gtest-demo-armhf.git
cd gtest-demo-armhf
mkdir build
cd build
cmake ..
make
```


## Running the tests

Either using `make test`:
```
$ make test

Running tests...
Test project /home/user/gtest-demo-armhf/build
    Start 1: unit
1/1 Test #1: unit .............................   Passed    0.00 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   0.00 sec
```

Or directly using `unit_tests`:
```
$ ./unit_tests

[==========] Running 2 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 2 tests from example
[ RUN      ] example.add
[       OK ] example.add (0 ms)
[ RUN      ] example.subtract
[       OK ] example.subtract (0 ms)
[----------] 2 tests from example (1 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 1 test case ran. (1 ms total)
[  PASSED  ] 2 tests.

```
